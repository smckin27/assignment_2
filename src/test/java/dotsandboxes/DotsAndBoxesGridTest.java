package dotsandboxes;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    /*
     * Because Test classes are classes, they can have fields, and can have static fields.
     * This field is a logger. Loggers are like a more advanced println, for writing messages out to the console or a log file.
     */
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);


    /*
    * A test to check if a box is correctly marked as complete.
    * Asserting True after drawing a complete box
    */


    @Test
    public void testForCompleteBox() {
        logger.info("Testing the complete box");
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(10,10,2);

        grid.drawHorizontal(1,1,1);
        grid.drawHorizontal(1,2,1);
        grid.drawVertical(1,1,1);
        grid.drawVertical(2,1,1);

        assertTrue(grid.boxComplete(1, 1));
    }


    /*
    * A test to see if a line can be redrawn after it has already been drawn,
    * test should throw an exception.
    */
    @Test
    public void testRedrawLineFails() {
        logger.info("Testing redrawing a line");

        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(10,10,2);

        grid.drawVertical(5,5,1);

        assertThrows(IllegalStateException.class, () -> grid.drawVertical(5, 5, 1));
    }
}
